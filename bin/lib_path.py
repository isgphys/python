#!/usr/bin/env python3

import os
import sys

script_path = os.path.dirname(os.path.realpath(__file__))
lib_path = os.path.abspath('/'.join([script_path, '..']))
sys.path.insert(1, lib_path)
