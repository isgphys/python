#!/usr/bin/env python3

import sys
import collections
import lib_path
import lib
import argparser
import dphysldap

OPTS = collections.OrderedDict()
OPTS['access'] = 'accessRight'
OPTS['blocked'] = 'blocked'
OPTS['home'] = 'homeDirectory'
OPTS['shell'] = 'loginShell'
OPTS['mail'] = 'mail'
OPTS['telephone'] = 'telephoneNumber'
OPTS['gid'] = 'gidNumber'
OPTS['cn'] = 'cn'
OPTS['language'] = 'language'


def main():
    description = 'Show user(s)'
    arg = 'user'
    arg_help = 'The user name (uid), or wildcards (*, uid* , *uid*)'
    args = argparser.simple(description, arg=arg, arg_help=arg_help, opts=OPTS)

    ldap = dphysldap.Ldap()

    attrs = ['uid', 'uidNumber', 'gecos']
    attrs.extend([v for k, v in OPTS.items() if args[k]])

    users = dphysldap.Users(ldap, attrs)
    users.search(args['user'])
    users.sort('uid')

    if not users:
        sys.exit('No users found.')

    if len(users) != 1:
        print(users)

    else:
        user = users[0]
        for attr in attrs:
            print(': '.join([attr, str(user[attr])]))


if __name__ == "__main__":
    main()
