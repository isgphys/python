#!/usr/bin/env python3

import sys
import collections
import lib_path
import lib
import argparser
import dphysldap

OPTS = collections.OrderedDict()
OPTS['access'] = 'accessRight'
OPTS['blocked'] = 'blocked'
OPTS['home'] = 'homeDirectory'
OPTS['shell'] = 'loginShell'
OPTS['report'] = ['reportEnabled', 'reportUid']
OPTS['mail'] = 'mail'
OPTS['telephone'] = 'telephoneNumber'

NOOPTS = collections.OrderedDict()
NOOPTS['no-member'] = 'memberUid'
NOOPTS['no-gid'] = 'gidNumber'
NOOPTS['no-owner'] = 'owner'
NOOPTS['no-uid'] = 'uidNumber'
NOOPTS['no-name'] = 'gecos'


def main():
    description = 'Show groups or group members'
    arg = 'group'
    arg_help = 'The group name (cn), or wildcards (*, cn* , *cn*)'
    parser = argparser.extendable(description, arg=arg, arg_help=arg_help, opts=OPTS, noopts=NOOPTS)
    parser.add_argument('--uids', dest='uid_only', action='store_const', const=True,
                        help='Show uids only')
    parser.add_argument('--names', dest='name_only', action='store_const', const=True,
                        help='Show real names only')
    args = vars(parser.parse_args())

    ldap = dphysldap.Ldap()

    group_attrs = ['cn', 'gidNumber', 'owner', 'memberUid']

    if args['report']:
        group_attrs.extend(OPTS['report'])

    groups = dphysldap.Groups(ldap, group_attrs)
    groups.search(args['group'])

    if not groups:
        sys.exit('No groups found.')

    if len(groups) != 1:
        groups.sort('cn')
        groups.remove_attrs([v for k, v in NOOPTS.items() if args[k]])
        print(groups)

    else:
        group = groups[0]
        members = group['memberUid']

        if not args['name_only'] and not args['uid_only']:
            print('Group {} ({}):'.format(group['cn'], group['gidNumber']))
            print('Owner: {}'.format(group['owner']))
            print('Report: {}'.format(group['reportEnabled']))
            print('Reported to: {}'.format(group['reportUid']))

        if members:
            del OPTS['report']

            attrs = ['uid', 'uidNumber', 'gecos']
            attrs.extend([v for k, v in OPTS.items() if args[k]])
            for attr in [v for k, v in NOOPTS.items() if args[k]]:
                if attr in attrs:
                    attrs.remove(attr)

            users = dphysldap.Users(ldap, attrs)
            users.search(';'.join(members))
            users.sort('uid')

            if args['uid_only']:
                for user in users:
                    print(user['uid'])
            elif args['name_only']:
                for user in users:
                    print(user['gecos'])
            else:
                print('Members:')
                print(users)

        else:
            print('Members: none')


if __name__ == "__main__":
    main()
