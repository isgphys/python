# python

This repo contains python scripts and modules which may not be available as package.

## installation

Clone the repo:

```
git clone git@gitlab.phys.ethz.ch:core/python.git
cd python
```

Install the requirements.

## requirements

### system

**Required** minimal packages:

```
apt install python3 python3-dev python3-setuptools python3-pip libacl1-dev libkrb5-dev
```

Optional packages:

```
apt install python3-venv
```

### git / pip

**Recommended** install from a clean venv (may be needed if you have any conflicting outdated modules installed as packages):

```
python3 -m venv venv
source venv/bin/activate
pip3 install --upgrade pip
pip3 install -r requirements-git.txt -t lib/git
pip3 install -r requirements-pip.txt -t lib/pip
deactivate
rm -r venv
```

**Alternative** modules in local lib directory (this will probably not work):

```
/usr/bin/pip3 install -r requirements-git.txt -t lib/git
/usr/bin/pip3 install -r requirements-pip.txt -t lib/pip
```


## modules

Store or load modules in these directories:

- `lib/isg/`: modules developed at isg dphys
- `lib/git/`: modules installed with git (github)
- `lib/pip/`: modules installed with pip

In your script or module the following 2 imports are required in order:

```python
import lib_path
import lib
```

In `ipython3` the following import is required:

```python
import lib
```

After that you can import modules from the lib paths, searched in the following order `isg` > `git` > `pip` > `system`.`

## scripts

Store scripts here:

- `bin/`

## feature request

If you would like to have a new feature, script or module, open an [issue](https://gitlab.phys.ethz.ch/core/python/issues/new).

- add **title** and  **description**
- choose **assignee**: the person who should do it or `Unassigned`
- choose **label**: `Feature request`
- submit

## bug report

To report a bug, open an [issue](https://gitlab.phys.ethz.ch/core/python/issues/new).

- add **title** and  **description**
- choose **assignee**: the person who should do it or `Unassigned`
- choose **label**: `Bug`
- submit

## contributing

This gitlab repo will be cloned to many locations, and the `master` branch should always be **stable**.

~~Therefore the `master` branch is protected. No one can push to it directly.~~ If you want someone else to review your changes it might be a good workflow to make a pull request (also for documentation purpose)

### branching

To contribute create your own **`local-<yourname>`** branch or a **feature/bugfix** branch:

```
git checkout -b newfeature
git push -u origin newfeature
```

Make your changes and push to your branch:

```
git add -A
git commit
git push
```

### pull request

After the push, git will show you the link to open a **pull request**:

```
remote: To create a merge request for local-rda, visit:
remote:   https://gitlab.phys.ethz.ch/core/python/merge_requests/new?merge_request%5Bsource_branch%5D=newfeature
```

You can also go to [branches](https://gitlab.phys.ethz.ch/core/python/branches) and click **Merge request**.

- add **description**: If it closes an issue write the issue nuber it fixes (`Fixes #1` / `Closes #1`)
- add **assignee** (optional)
- add **label**: `Feature` or `Fix`
- submit

Let someone review it and discuss. **Changes** shows the diff.

### merge

Once it is ready to be merged:

- merge it by clicking the green **Merge** button
- remove source branch now with the **Remove Source Branch** button (optional)

### pull in feature branch

If you want to continue working in your branch, do:

```
git checkout newfeature
git pull --rebase origin master
```

This will do a fast-forward merge.

### pull in `master` branch

```
git checkout master
```

Pull the updated `master` branch (which now contains your work):

```
git pull
```

A `pull` actually does this:

```
git fetch origin
git merge origin/master
```

### rebase branch

If you decide to make another commit in this branch, rebase first:

```
git checkout newfeature
git rebase origin/master
```

This will do a fast-forward merge.

### delete feature branch

You could now also safely remove your feature branch in your own git repo:

```
git branch -d newfeature
```

