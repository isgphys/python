#!/usr/bin/env python3

import ssl
import collections
import time
import lib_path
import lib
import ldap3
import tabulate

FMT = 'simple'
SERVERS = ['phd-aa1.ethz.ch', 'phd-aa2.ethz.ch', 'phd-aa3.ethz.ch']
BASE = 'dc=phys,dc=ethz,dc=ch'
CA_CERTS = '/etc/ssl/certs/ca-certificates.crt'
SLAPD_SOCKET = 'ldapi:///var/run/slapd/ldapi'


class AttributeValue(list):
    """
    Abstraction class for LDAP attribute values imitating list
    """
    def __str__(self):
        """
        Modifies the "informal" string value of str(x) or print(x)
        """
        return self.get_str()

    def get_str(self, delimit=','):
        """
        Returns a string representation of all values
        """
        return delimit.join([str(e) for e in self])


class Entry(collections.abc.MutableMapping):
    """
    Abstraction class for LDAP Entry imitating dict
    """
    def __init__(self, *args, **kwargs):
        self._dict = dict()
        self.update(dict(*args, **kwargs))

    def __getitem__(self, key):
        return self._dict[key]

    def __setitem__(self, key, value):
        """
        If value is a list, convert it to AttributeValue
        """
        if isinstance(value, list):
            self._dict[key] = AttributeValue(value)
        else:
            self._dict[key] = value

    def __delitem__(self, key):
        del self._dict[key]

    def __iter__(self):
        return iter(self._dict)

    def __len__(self):
        return len(self._dict)

    def __str__(self):
        """
        Modifies the "informal" string value of str(x) or print(x)
        """
        return '\n'.join([': '.join([k, str(v)]) for k, v in self.items() if v])


class Group(Entry):
    """
    Abstraction class for LDAP Group imitating Entry
    """
    def __init__(self, *args, **kwargs):
        Entry.__init__(self, *args, **kwargs)
        if 'owner' in self:
            for i, owner in enumerate(self['owner']):
                self['owner'][i] = owner.split('=', 1)[1].split(',', 1)[0]


class User(Entry):
    """
    Abstraction class for LDAP User imitating Entry
    """
    def __init__(self, *args, **kwargs):
        Entry.__init__(self, *args, **kwargs)


class Slapd(object):
    """
    SLAPD connection via socket SASL EXTERNAL authenticated
    """
    def __init__(self, socket=SLAPD_SOCKET, get_info=ldap3.ALL):
        self.socket = socket
        self.get_info = get_info

    def connect(self):
        """
        Connect to slapd and bind as current user
        """
        self.server = ldap3.Server(self.socket, get_info=self.get_info)
        self.connection = ldap3.Connection(
            self.server,
            authentication=ldap3.SASL,
            sasl_mechanism=ldap3.EXTERNAL,
            sasl_credentials='',
            auto_bind='NONE',
            version=3,
            client_strategy=ldap3.SYNC)
        self.connection.bind()

    def connect_retry(self, interval=1, retries=0):
        """
        Retry connection every interval until no retries left or forever
        """
        forever = True if not retries else False

        while forever or retries > 0:
            try:
                self.connect()
                return True
            except ldap3.core.exceptions.LDAPSocketOpenError:
                retries -= 1
                time.sleep(interval)

        return False

    def search(self, search_base, search_filter, search_scope=ldap3.SUBTREE,
               attributes=[ldap3.ALL_ATTRIBUTES, ldap3.ALL_OPERATIONAL_ATTRIBUTES]):
        """
        LDAP search operation
        """
        response = self.connection.search(search_base, search_filter,
                                          search_scope=search_scope,
                                          attributes=attributes)
        return response

    def response(self):
        """
        Get search operation response
        """
        return self.connection.response

    def get_byte_syntaxes(self):
        """
        Read the schema and extract byte syntaxes
        """
        byte_syntaxes = set()
        byte_syntaxes.add('1.3.6.1.4.1.1466.115.121.1.40')

        if self.server.schema:
            for key, value in self.server.schema.ldap_syntaxes.items():
                if value.extensions:
                    for ext in value.extensions:
                        if ext[0] == 'X-NOT-HUMAN-READABLE' and ext[1][0] == 'TRUE':
                            byte_syntaxes.add(key)

        return byte_syntaxes

    def get_byte_attributes(self, byte_syntaxes=None):
        """
        Read the schema and extract byte attributes
        """
        byte_attrs = list()

        if not byte_syntaxes:
            byte_syntaxes = self.get_byte_syntaxes()

        if self.server.schema:
            for key, value in self.server.schema.attribute_types.items():
                if value.syntax in byte_syntaxes:
                    byte_attrs.append(key)

        return byte_attrs

class Ldap(object):
    """
    LDAP connection to random server in pool
    """
    def __init__(self, server_names=SERVERS, base=BASE, ca_certs_file=CA_CERTS):
        self.server_names = server_names
        self.base = base
        self.tls = ldap3.Tls(
            validate=ssl.CERT_REQUIRED,
            version=ssl.PROTOCOL_TLSv1_2,
            ca_certs_file=ca_certs_file)
        self.servers = [ldap3.Server(s, tls=self.tls, get_info=ldap3.ALL) for s in self.server_names]
        self.server_pool = ldap3.ServerPool(
            self.servers,
            pool_strategy=ldap3.RANDOM,
            active=True,
            exhaust=False)
        self.connection = ldap3.Connection(
            self.server_pool,
            authentication='ANONYMOUS',
            auto_bind='NONE',
            version=3,
            client_strategy='SYNC')
        self.connection.open()
        self.connection.start_tls()
        self.connection.bind()
        self.user_classes = ['posixAccount', 'dphysUser', 'inetOrgPerson', 'shadowAccount']
        self.group_classes = ['posixGroup', 'dphysGroup']
        self.obj_user = None
        self.obj_group = None

    def get_object(self, classes):
        """
        Get an objectDef object from a list of objectClasses
        """
        return ldap3.ObjectDef(classes, self.connection)

    def get_entries(self, obj=None, classes=None, base=None, query='', attributes=None):
        """
        Returns a list with entries as dict
        """
        if base is None:
            base = self.base
        if obj is None:
            obj = self.get_object(classes)
        reader = ldap3.Reader(self.connection, obj, base, query, attributes)
        reader.search()
        return [Entry(e.entry_attributes_as_dict) for e in reader.entries]

    def get_users(self, query='', attributes=None):
        """
        Returns a list with users as dict
        """
        if not self.obj_user:
            self.obj_user = self.get_object(self.user_classes)
        entries = self.get_entries(obj=self.obj_user, query=query, attributes=attributes)
        return [User(e) for e in entries]

    def get_groups(self, query='', attributes=None):
        """
        Returns a list with groups as dict
        """
        if not self.obj_group:
            self.obj_group = self.get_object(self.group_classes)
        entries = self.get_entries(obj=self.obj_group, query=query, attributes=attributes)
        return [Group(e) for e in entries]

    def search(self, search_filter, search_base=None, search_scope=ldap3.SUBTREE,
               attributes=[ldap3.ALL_ATTRIBUTES, ldap3.ALL_OPERATIONAL_ATTRIBUTES]):
        """
        LDAP search operation
        """
        if not search_base:
            search_base = self.base
        response = self.connection.search(search_base, search_filter,
                                          search_scope=search_scope,
                                          attributes=attributes)
        return response

    @property
    def response(self):
        """
        Get search operation response
        """
        return self.connection.response


class Entries(list):
    """
    Abstraction class for Entries imitating list
    """
    def __init__(self, ldap, attrs, *args):
        list.__init__(self, *args)
        self._ldap = ldap
        self.attrs = attrs

    def __str__(self):
        """
        Modifies the "informal" string value of str(x) or print(x)
        to return entries in tabulated form
        """
        table = [[e[a] for a in self.attrs] for e in self]
        return tabulate.tabulate(table, tablefmt=FMT, headers=self.attrs)

    def sort(self, attr):
        super().sort(key=lambda k: str(k[attr]))

    def remove_attr(self, attr):
        if attr in self.attrs:
            self.attrs.remove(attr)

    def remove_attrs(self, attrs):
        if isinstance(attrs, list):
            for attr in attrs:
                self.remove_attr(attr)
        else:
            self.remove_attr(attrs)

    def search(self, query, classes=None, obj=None, base=None):
        """
        Query syntax: `<attributeName>: <attributeValue(s)>`
        AttributeValue: `a`, `a*`, `*a*`, `a;b`
        """
        self.clear()
        self.extend(self._ldap.get_entries(obj=obj, classes=classes, base=base, query=query, attributes=self.attrs))


class Groups(Entries):
    """
    Abstraction class for Groups imitating list
    """
    def search(self, cn):
        """
        Search example: `cn`, `cn*`, `*cn*`, `cn1;cn2`
        """
        query = 'cn: {0}'.format(cn)
        self.clear()
        self.extend(self._ldap.get_groups(query=query, attributes=self.attrs))

    def members(self, index=None):
        """
        Returns list of members
        """
        if index == None:
            members = set()
            for group in self:
                members.add(group['memberUid'])
            return list(members)
        else:
            return self[index]['memberUid']


class Users(Entries):
    """
    Abstraction class for Users imitating list
    """
    def search(self, uid):
        """
        Search example: `uid`, `uid*`, `*uid*`, `uid1;uid2`
        """
        query = 'uid: {0}'.format(uid)
        self.clear()
        self.extend(self._ldap.get_users(query=query, attributes=self.attrs))
