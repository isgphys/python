#!/usr/bin/env python3

import argparse


def extendable(description, arg=None, arg_help='', opts=dict(), noopts=dict()):
    """
    Get extendable simplified argparser
    """
    parser = argparse.ArgumentParser(
        add_help=False, description=description)

    if arg:
        parser.add_argument(arg, help=arg_help)

    for k, v in opts.items():
        if isinstance(v, list):
            v = ', '.join(v)

        try:
            parser.add_argument('-' + k[:1], '--' + k, dest=k,
                                action='store_const', const=True,
                                help='Show ' + v)
        except argparse.ArgumentError:
            parser.add_argument('--' + k, dest=k,
                                action='store_const', const=True,
                                help='Show ' + v)

    for k, v in noopts.items():
        if isinstance(v, list):
            v = ', '.join(v)

        try:
            parser.add_argument('-' + k[3:4].upper(), '--' + k, dest=k,
                                action='store_const', const=True,
                                help='Hide ' + v)
        except argparse.ArgumentError:
            parser.add_argument('--' + k, dest=k,
                                action='store_const', const=True,
                                help='Hide ' + v)

    parser.add_argument('--help', action='help',
                        help='Show this help message and exit')

    return parser


def simple(description, arg=None, arg_help='', opts=dict(), noopts=dict()):
    """
    Get cmd argument with simplified argparser
    """
    parser = extendable(description, arg=arg, arg_help=arg_help, opts=opts, noopts=noopts)

    return vars(parser.parse_args())
