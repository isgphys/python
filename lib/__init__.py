#!/usr/bin/env python3

import os
import sys

lib_path = os.path.dirname(os.path.realpath(__file__))

lib_pip = '/'.join([lib_path, 'pip'])
lib_git = '/'.join([lib_path, 'git'])
lib_isg = '/'.join([lib_path, 'isg'])

sys.path.insert(1, lib_pip)
sys.path.insert(1, lib_git)
sys.path.insert(1, lib_isg)
